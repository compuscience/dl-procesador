library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity alu_tb is
end alu_tb;

architecture practica1 of alu_tb is
  
  component ALU
    port (a , b : in std_logic_vector(7 downto 0);
      op : in std_logic_vector (2 downto 0);
      s : out std_logic_vector (7 downto 0);
      oflow : out std_logic);
    end component;
    
    --estimulos
  signal a, b : std_logic_vector(7 downto 0);
  signal op : std_logic_vector (2 downto 0);
  
  --observacion
  signal s : std_logic_vector (7 downto 0);
  signal oflow : std_logic;
  
  
  constant delay: time:= 10 ns;
  
  
  begin
    UUT : ALU port map(
      a => a,
      b => b,
      op => op,
      s => s,
      oflow => oflow);
       
     process
        variable op_cod : unsigned(2 downto 0) := "000"; 
        begin 
        a <= "10101010";
        b <= "11010101";
        op <= std_logic_vector(op_cod);
        op_cod := op_cod + "001";
        wait for 2 * delay;
      end process;
      
    end practica1;
