library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity proc is
    port ( clk : in  std_logic;
           rst : in  std_logic;
           input : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out  std_logic_vector (7 downto 0));
end proc;



--ARQUITECTURA DEL PROCESADOR

architecture comp_proc of proc is

-- ================================================
-- Declaraci�n de los componentes utilziados
-- ================================================

component regs 
    port ( clk : in  std_logic;
           rst : in  std_logic;
           we : in  std_logic;
           rd : in  std_logic_vector (3 downto 0);
           rs : in  std_logic_vector (3 downto 0);
           din : in  std_logic_vector (7 downto 0);
           dout : out  std_logic_vector (7 downto 0));
end component;

component reg
  port ( clk    : in  STD_LOGIC;
         rst    : in  STD_LOGIC;
         we     : in  STD_LOGIC;
         input  : in  STD_LOGIC_VECTOR (7 downto 0);
         output : out STD_LOGIC_VECTOR (7 downto 0));
         end component;
         
component alu 
    port ( op: in  std_logic_vector(2 downto 0);
           a,b : in  std_logic_vector (7 downto 0);
           s : out  std_logic_vector (7 downto 0));
end component;

component rom_prog 
    port (addr : in  std_logic_vector (6 downto 0);
				  rom_out : out  std_logic_vector (15 downto 0));
end component; 


component decode
   port (deco_input : in  std_logic_vector (7 downto 0);
         out_we: out std_logic;
         reg_we: out std_logic;
         reg_a_we: out std_logic;
         bus_sel : out std_logic_vector (1 downto 0);
         alu_op : out std_logic_vector (2 downto 0));
end component; 

component ir
  port ( clk    : in  STD_LOGIC;
           rst    : in  STD_LOGIC;
           input  : in  STD_LOGIC_VECTOR (15 downto 0);
           output : out STD_LOGIC_VECTOR (15 downto 0));
  end component;
  
  component pc
    port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           output : out  STD_LOGIC_VECTOR (6 downto 0));
  end component;

component mux
  port(a, b, c: in STD_LOGIC_VECTOR(7 downto 0);
        sel: in STD_LOGIC_VECTOR(1 downto 0);
        output: out STD_LOGIC_VECTOR(7 downto 0));
end component;
 
-- ================================================
-- Declaraci�n de se�ales usadas 
-- ================================================

-- Banco de registros
signal dout : std_logic_vector(7 downto 0); --se�ales de datos que se almacenan o se utilizan de los registros

-- Decoder
signal out_we, reg_we, reg_a_we : std_logic; --se�al que habilita la escritura en el registro de salida
signal alu_op: std_logic_vector(2 downto 0); --se�al que indica que proceso ejecutara la ALU
signal bus_sel:std_logic_vector(1 downto 0); --selector que indica que se�al se aplicara a las entradas de la ALU

--BUS de salida
signal BUS_OUT : std_logic_vector(7 downto 0); --se�al de salida de la ALU

--Registro_A
signal reg_a_out : std_logic_vector(7 downto 0); --se�al de salida registro A

--BUS de entrada
signal BUS_IN : std_logic_vector(7 downto 0);

--Se�ales para almacenamiento
signal pc_out : std_logic_vector (6 downto 0); --se�al que toma el valor del PC por cada ciclo
signal ir_out : std_logic_vector (15 downto 0); --se�al de salida del registro IR

--Rom
signal rom_out : std_logic_vector (15 downto 0); --se�al de salida de la rom

-- ================

begin

-- ================
-- Instanciacion de componentes utilizados

-- Banco de registros
eregs: regs port map (clk => clk, rst => rst, we => reg_we, 
								rd => ir_out(7 downto 4), rs => ir_out(3 downto 0), 
								din => BUS_OUT, dout=>dout );
-- ALU
eAlu: alu port map (a => BUS_IN, b => reg_a_out, op => alu_op, s => BUS_OUT);

-- ROM de programa
eROM_Prog: rom_prog port map (addr => pc_out, rom_out => rom_out) ;

-- El decodificador de la instrucci�n
eDecode: decode port map (deco_input => ir_out(15 downto 8), reg_we =>reg_we,alu_op => alu_op,
                              reg_a_we => reg_a_we, bus_sel => bus_sel, out_we => out_we );
--Ir
eIr: ir port map (clk => clk, rst => rst, input => rom_out , output => ir_out);

--Pc  
ePc: pc port map (clk => clk, rst => rst, output => pc_out);

--Reg_A
reg_A: reg port map( clk => clk, rst => rst,we => reg_a_we, input => BUS_IN,
           output => reg_a_out);
--Reg_Out
reg_Out: reg port map( clk => clk, rst => rst,we => out_we, input => BUS_OUT,
           output => output);--Falta definir el segundo bus$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
--MUX
eMux : mux port map ( a => dout, b => ir_out(7 downto 0), c => input, sel => bus_sel, output => BUS_IN);
  
-- ================
end comp_proc;