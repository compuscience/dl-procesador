library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_signed.ALL;

--Descripcion de entidad entradas y salidas

entity ALU is
  port(a, b: in std_logic_vector(7 downto 0);
        op: in std_logic_vector(2 downto 0);
        s: out std_logic_vector(7 downto 0);
        overflow: out std_logic);
end ALU;

--arquitectura basada en sentencias secuenciales "case"
architecture proc_alu of ALU is
  begin
    process(a, b, op)
      VARIABLE result_temp : std_logic_vector(7 downto 0);
      begin

      case op is
        when "001" => s <= a(6 downto 0) & "0";
          
        when "010" => 
        result_temp := a+b;
        IF (a(7)=b(7) AND result_temp(7)/=a(7)) THEN overflow<='1'; s <= "00000000"; 
        ELSE
        s <= result_temp;
        END IF;
                    
        when "011" => 
        result_temp := a-b;
        IF (a(7)/=b(7) AND result_temp(7)/=a(7)) THEN overflow<='1'; s <= "00000000";
        ELSE
        s <= result_temp;
        END IF;
        
        
        when "100" => s <= a and b;
          
        when "101" => s <= a or b;
          
        when "110" => s <= a xor b;
          
        when "111" => s <= "0" & a(7 downto 1);
          
        when others => s <=a;
      end case;
    end process;
    
  end proc_alu;        
