library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.ALL;

entity decode is
  port(deco_input : in std_logic_vector(7 downto 0);
       bus_sel: out std_logic_vector(1 downto 0);
       alu_op : out std_logic_vector(2 downto 0);
       reg_a_we : out std_logic;
       out_we : out std_logic;
       reg_we : out std_logic); 
       end decode;
       

architecture comp_decod of decode is
  begin
  process(deco_input)
  begin
    case deco_input is
    when "00000001" => bus_sel<= "10";
                       alu_op <= "000";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x01 IN
    when "00000010" => bus_sel<= "00";
                       alu_op <= "000";
                       reg_a_we <= '0';
                       out_we <= '1';
                       reg_we <= '0'; --Instruccion x02 OUT
    when "00000011" => bus_sel<= "00";
                       alu_op <= "000";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x03 MOV
    when "00000100" => bus_sel<= "00";
                       alu_op <= "000";
                       reg_a_we <= '1';
                       out_we <= '0';
                       reg_we <= '0'; --Instruccion x04 LDA
    when "00000101" => bus_sel<= "01";
                       alu_op <= "000";
                       reg_a_we <= '1';
                       out_we <= '0';
                       reg_we <= '0'; --Instruccion x05 LDI
    when "00001010" => bus_sel<= "00";
                       alu_op <= "010";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x10 ADD
    when "00001011" => bus_sel<= "00";
                       alu_op <= "011";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x11 SUB
    when "00001100" => bus_sel<= "00";
                       alu_op <= "100";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x12 AND
    when "00001101" => bus_sel<= "00";
                       alu_op <= "101";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x13 OR
    when "00001110" => bus_sel<= "00";
                       alu_op <= "110";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x14 XOR
    when "00010100" => bus_sel<= "00";
                       alu_op <= "001";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x20 SHL
    when "00010101" => bus_sel<= "00";
                       alu_op <= "111";
                       reg_a_we <= '0';
                       out_we <= '0';
                       reg_we <= '1'; --Instruccion x21 SHR
    when others     =>bus_sel<= "00";
                      alu_op <= "000";
                      reg_a_we <= '0';
                      out_we <= '0';
                      reg_we <= '0'; --resto de los casos la respuesta es todo 0    
    end case;
  end process;
end comp_decod;