
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Reg_TB is
end Reg_TB;

architecture comp_reg of Reg_TB is

  component Reg is
     Port (clk    : in  STD_LOGIC;
           rst    : in  STD_LOGIC;
           we     : in  STD_LOGIC;
           input  : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR (7 downto 0));
  end component; 

  signal clk    : STD_LOGIC;
  signal rst    : STD_LOGIC;
  signal we     : STD_LOGIC;
  signal input  : STD_LOGIC_VECTOR (7 downto 0);
  signal output : STD_LOGIC_VECTOR (7 downto 0);
  
begin 
  Mi_Reg : Reg port map (clk => clk, 
                         rst => rst,
                         we => we,
                         input => input, 
                         output => output);

  Clock : process
  begin
    clk <= '0';
    wait for 20 ns;
    clk <= '1';
    wait for 20 ns;
  end process;
 
  Test : process
  begin
    rst <= '1';
    input <= "01010101";
    we <= '1';
    wait until (clk'event and clk = '1');
    assert output = "00000000" report "Reset no funcion�";
    rst <= '0';
    wait until (clk'event and clk = '1');
    input <= "10101010";
    we <= '0';
    wait until (clk'event and clk = '1');
    assert output = "01010101" report "No guard�";
    wait until (clk'event and clk = '1');
    assert output = "01010101" report "Guard� sin habilitaci�n";
    wait;
  end process; 
  
end comp_reg;


