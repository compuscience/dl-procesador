library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity rom_tb is
end rom_tb;

architecture test of rom_tb is
  component rom_prog
     port (addr : in  std_logic_vector(6 downto 0);
      rom_out: out std_logic_vector(15 downto 0));     
  end component;

--estimulo
signal addr : std_logic_vector(6 downto 0);
--salida
signal rom_out :  std_logic_vector(15 downto 0);

constant delay: time:= 10 ns;


begin
  mi_rom : rom_prog
      port map (addr => addr 
                ,rom_out => rom_out);
                
      process
        begin
         addr <= "0000000";
         wait for 10 ns;
         assert rom_out = x"0130" report "falla" severity failure;
         addr <= "0000001";
         wait for 10 ns;
         assert rom_out = x"0403" report "falla" severity failure; 
         addr <= "0000010";
         wait for 10 ns;
         assert rom_out = x"1043" report "falla" severity failure; 
         addr <= "0000011";
         wait for 10 ns; 
         assert rom_out = x"1154" report "falla" severity failure;
         addr <= "0000100";
         wait for 10 ns; 
         assert rom_out = x"1364" report "falla" severity failure;
         addr <= "0000101";
         wait for 10 ns;
         assert rom_out = x"1270" report "falla" severity failure; 
         addr <= "1111111";
         wait for 10 ns; 
         assert rom_out = x"0000" report "falla" severity failure;
         
      end process; 

end test;