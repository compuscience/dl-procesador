
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Reg is
    Port ( clk    : in  STD_LOGIC;
           rst    : in  STD_LOGIC;
           we     : in  STD_LOGIC;
           input  : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR (7 downto 0));
end Reg;

architecture comp_reg of Reg is
  
  signal reg : STD_LOGIC_VECTOR (7 downto 0);
  
begin 

  process (clk, rst)
  begin
    if rst = '1' then
      reg <= (others => '0');
    elsif (clk'event and clk = '1' and we = '1') then
      reg <= input;
    end if; 
  end process; 

  output <= reg;
  
end comp_reg;



