
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PC is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           output : out  STD_LOGIC_VECTOR (6 downto 0));
end PC;

architecture comp_pc of PC is
  
  signal counter : UNSIGNED (6 downto 0);
  
begin 

  process (clk, rst)
  begin
    if rst = '1' then
      counter <= (others => '0');
    elsif (clk'event and clk = '1' and counter /= "1111111") then
      counter <= counter + 1;
    end if; 
  end process; 

  output <= STD_LOGIC_VECTOR(counter);
  
end comp_pc;


