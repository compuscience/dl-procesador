library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--Definicion de entradas y salidas

entity rom_prog is
    port (
        addr : in  std_logic_vector(6 downto 0);
        rom_out : out std_logic_vector(15 downto 0));
end rom_prog;

--Arquitectura de la ROM: aqui van definidas las instrucciones a ejecutarse por el procesador

architecture comp_rom of rom_prog is
  type rom is array(0 to 127) of STD_LOGIC_VECTOR(15 downto 0);
  signal programa: rom := (x"0130",x"0403",x"0A43", x"0B54",x"0D64",x"0C70",
                           x"03E4", x"0203", x"0204", x"0205",x"0206",
                           x"0207",x"0208", x"020D", x"020E" ,others => x"0000");
   --aqui van los codigos de todas las instrucciones a ejecutarse
  
  begin
  process (addr) 
  begin
       rom_out <= programa(conv_integer(addr));
  end process; 

  end comp_rom;
  
  
  
  
  
  