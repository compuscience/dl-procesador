library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
  port(a, b, c: in STD_LOGIC_VECTOR(7 downto 0);
        sel: in STD_LOGIC_VECTOR(1 downto 0);
        output: out STD_LOGIC_VECTOR(7 downto 0));
end mux;

architecture proc_mux of mux is
  begin
    output <= c when sel(1) = '1' else
              b when sel(0) = '1' else
              a;
    
  end proc_mux;        