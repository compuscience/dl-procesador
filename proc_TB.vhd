library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity proc_tb is
end proc_tb;

architecture test of proc_tb is
  component proc
     port (clk : in  std_logic;
           rst : in  std_logic;
           input : in  STD_LOGIC_VECTOR (7 downto 0);
           output : out  std_logic_vector (7 downto 0));   
  end component;

--estimulo
signal input : std_logic_vector(7 downto 0);
signal rst: std_logic;
signal clk: std_logic := '0';
constant clk_period : time := 100 ns;
--salida
signal output: std_logic_vector (7 downto 0);



begin
  procesador : proc
      port map (input => input, 
                rst => rst,
                clk => clk,
                output => output);  
                
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 50 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 50 ns signal is '1'.
   end process; 

   rst_process :process
   begin
        rst <= '1';
        wait for clk_period;
        rst <= '0';
        wait;
   end process; 
                
   test : process
   begin
         input <= "00000001"; 
         wait for clk_period; 
         assert output = "00000000" report "Falla el reset";
         --resto de los casos
         wait for clk_period;
         assert output = "00000000"  report "Falla resultado luego de instruccion 0";
         wait for clk_period;
         assert output = "00000000"  report "Falla resultado luego de instruccion 1";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 2";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 3";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 4";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 5";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 6";
         wait for clk_period;
         assert output = "00000001"  report "falla resultado luego de instruccion 7";
         wait for clk_period;
         assert output = "00000010"  report "falla resultado luego de instruccion 8";
         wait for clk_period;
         assert output = "00000001"  report "falla resultado luego de instruccion 9";
         wait for clk_period;
         assert output = "00000011"  report "falla resultado luego de instruccion 10";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 11";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 12";
         wait for clk_period;
         assert output = "00000000"  report "falla resultado luego de instruccion 13";
         wait for clk_period;
         assert output = "00000010"  report "falla resultado luego de instruccion 14";
         for I in 15 to 127 loop
           wait for clk_period;
           assert output = "00000010"  report "falla resultado luego de instruccion " & INTEGER'IMAGE(I);
         end loop;
         wait;
   end process; 
end test;