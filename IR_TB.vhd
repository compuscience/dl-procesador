
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IR_TB is
end IR_TB;

architecture comp_ir of IR_TB is

  component IR is
     Port (clk    : in  STD_LOGIC;
           rst    : in  STD_LOGIC;
           input  : in  STD_LOGIC_VECTOR (15 downto 0);
           output : out STD_LOGIC_VECTOR (15 downto 0));
  end component; 

  signal clk    : STD_LOGIC;
  signal rst    : STD_LOGIC;
  signal input  : STD_LOGIC_VECTOR (15 downto 0);
  signal output : STD_LOGIC_VECTOR (15 downto 0);
  
begin 
  Mi_IR : IR port map (clk => clk, rst => rst, input => input, output => output);

  Clock : process
  begin
    clk <= '0';
    wait for 20 ns;
    clk <= '1';
    wait for 20 ns;
  end process;
 
  Test : process
  begin
    rst <= '1';
    input <= "0000000011111111";
    wait until (clk'event and clk = '1');
    assert output = "0000000000000000" report "Reset no funcion�";
    rst <= '0';
    wait until (clk'event and clk = '1');
    input <= "1111111100000000";
    wait until (clk'event and clk = '1');
    assert output = "0000000011111111" report "No carg� primera instrucci�n";
    wait until (clk'event and clk = '1');
    assert output = "1111111100000000" report "No carg� segunda instrucci�n";
    wait;
  end process; 
  
end comp_ir;



