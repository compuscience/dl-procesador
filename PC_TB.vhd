
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PC_TB is
end PC_TB;

architecture comp_pc of PC_TB is

  component PC is
      port ( clk : in  STD_LOGIC;
             rst : in  STD_LOGIC;
             output : out  STD_LOGIC_VECTOR (6 downto 0));
  end component; 

  signal clk : STD_LOGIC;
  signal rst : STD_LOGIC;
  signal output : STD_LOGIC_VECTOR (6 downto 0);  
  
begin 
  Mi_PC : PC port map (clk => clk, rst => rst, output => output);

  Clock : process
  begin
    clk <= '0';
    wait for 20 ns;
    clk <= '1';
    wait for 20 ns;
  end process;
 
  Test : process
  begin
    rst <= '1';
    wait until (clk'event and clk = '1');
    assert output = "0000000" report "Reset no funcion�";
    rst <= '0';
    wait until (clk'event and clk = '1');
    for C in 0 to 127 loop
      assert output = STD_LOGIC_VECTOR(TO_UNSIGNED(C,7)) report "No increment�";
      wait until (clk'event and clk = '1');
    end loop;
    assert output = "1111111" report "Contador se reinicia";
    wait;
  end process; 
  
end comp_pc;

