library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity deco_tb is
end deco_tb;

architecture test of deco_tb is
  component decode
    port(deco_input : in std_logic_vector(7 downto 0);
       bus_sel: out std_logic_vector(1 downto 0);
       alu_op : out std_logic_vector(2 downto 0);
       reg_a_we : out std_logic;
       out_we : out std_logic;
       reg_we : out std_logic);     
  end component;

--estimulo
signal deco_input : std_logic_vector(7 downto 0);
--salidas
signal alu_op  : std_logic_vector(2 downto 0);
signal bus_sel : std_logic_vector(1 downto 0);
signal reg_a_we: std_logic;
signal out_we  : std_logic;
signal reg_we  : std_logic;

begin
  deco : decode
      port map (deco_input => deco_input, 
                bus_sel => bus_sel,
                alu_op => alu_op,
                reg_a_we => reg_a_we,
                reg_we => reg_we,
                out_we => out_we);
                
      process
        begin
         deco_input <= "00000001";--Instruccion x01 IN
         wait for 10 ns;
         assert alu_op = "000" and bus_sel = "10" and reg_a_we = '0' and
         reg_we = '1' and out_we = '0' report "falla" severity failure;         

         deco_input <= "00000010";--Instruccion x02 OUT
         wait for 10 ns;
         assert alu_op = "000" and bus_sel = "00" and reg_a_we = '0' and
         reg_we = '0' and out_we = '1' report "falla" severity failure;

         deco_input <= "00000011";--Instruccion x03 MOV
         wait for 10 ns;
         assert alu_op = "000" and bus_sel = "00" and reg_a_we = '0' and
         reg_we = '1' and out_we = '0' report "falla" severity failure;

         deco_input <= "00000100";--Instruccion x04 LDA
         wait for 10 ns; 
         assert alu_op = "000" and bus_sel = "00" and reg_a_we = '1' and
         reg_we = '0' and out_we = '0' report "falla" severity failure;

         deco_input <= "00000101";--Instruccion x05 LDI
         wait for 10 ns; 
         assert alu_op = "000" and bus_sel = "01" and reg_a_we = '1' and
         reg_we = '0' and out_we = '0' report "falla" severity failure;

         deco_input <= "00001010";--Instruccion x10 ADD
         wait for 10 ns;
         assert alu_op = "010" and bus_sel = "00" and reg_a_we = '0' and
         reg_we = '1' and out_we = '0' report "falla" severity failure;

         deco_input <= "11111111";--Instruccion no valida(se espera todo en 0)
         wait for 10 ns; 
         assert alu_op = "000" and bus_sel = "00" and reg_a_we = '0' and
         reg_we = '0' and out_we = '0' report "falla" severity failure;
         
      end process; 

end test;
